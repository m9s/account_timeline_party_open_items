# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Party Open Items',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Parteien Offene-Posten-Liste',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Party Open Items
    - Adds the timeline concept for Account Party Open Item
''',
    'description_de_DE': '''Gültigkeitsdauer für OPOS-Liste
    - Stellt die Merkmale der Gültigkeitsdauermodule für Parteien
      Offene-Posten-Liste zur Verfügung
''',
    'depends': [
        'account_party_open_items',
        'account_timeline_invoice'
    ],
    'xml': [
    ],
    'translation': [
    ],
}
