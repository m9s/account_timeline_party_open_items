#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class OpenItemListAsk(ModelView):
    _name = 'account.open_item_list.init'

    def default_accounts(self):
        account_obj = Pool().get('account.account')
        fiscalyear_obj = Pool().get('account.fiscalyear')

        fiscalyear = fiscalyear_obj.get_fiscalyear()
        args = [
            ('reconcile', '=', True),
            ('party_is_mandatory', '=', True),
            ('fiscalyear', '=', fiscalyear.id)
            ]
        res = account_obj.search(args)
        return res

OpenItemListAsk()


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    def _get_accounts_report_open_items(self, account_ids=None):
        account_obj = Pool().get('account.account')

        res = []
        if account_ids:
            res.extend(account_ids)
            accounts = account_obj.browse(account_ids)
            for account in accounts:
                res.extend([x.id for x in account.all_predecessors])
        else:
            res = account_obj.search([('reconcile', '=', True)])
        return res

Account()
